---
layout: page-with-outline.liquid
pageTitle: How it works
pageSubtext: Explanation of our concepts and methods
outline: 
    - {url: '#introduction', title: 'Introduction'}
    - {url: '#basic-concepts', title: 'Basic Concepts'}
    - {url: '#calculation', title: 'Calculation'}
    - {url: '#impact-categories', title: 'Impact Categories'}
    - {url: '#component-materials', title: 'Data for Component Materials'}
    - {url: '#material-countries', title: 'Data for Materials from Countries'}
    - {url: '#future-developments', title: 'Future Developments'}
---
<h2 id="introduction">Introduction</h2>
<p>
By analyzing the raw materials contained within a device, we pinpoint the global hotspots where human rights violations are most likely to occur.
Our goal is to provide a risk analysis that highlights the areas that should be focused for further activities, like further data collection, audits of suppliers, 
or exploration of design alternatives. 
In the following, we describe the basic concepts, assumptions, calculations and limitations behind Fairtronics.
</p>

<h2 id="basic-concepts">Basic Concepts</h2>

<h3>Electronics Products consist of Components</h3>

When you create a social impact report with Fairtronics you can configure an electronics <b>product</b> from our <b>component</b> database,
where we collect data about electronics components (like resistors, circuit boards, cables, ...). While we strive for completeness, especially in the beginning, 
you might have to select a component that fits "good enough" to the one that is actually part of your product.

<h3>Components are made from Raw Materials</h3>

For each <b>component</b> in our database we collect data about its total weight and the share in weight of the raw <b>materials</b> it is made of.
Again, while we strive for completeness, in the beginning we focus on metals. You should be aware that weight is only a proxy for what
we are actually interested in: working hours or potential for human harm. As long as we are missing better data sources, we assume that 
material weight corresponds to the human effort and therefore potential of harm that can be associated with this material.

<h3>Raw Materials are produced in different Countries</h3>

For each raw material (iron, copper, silver, ...), we collect data about world production share of different countries.
So, in effect, we assume that for example copper in one component in your product is a mix of all copper sources in the world. 
Initially, we focus on collecting data about metals.

<h3>Countries show different risks concerning Social Impacts</h3>

Global institutions like the International Labor Organization or the Unicef provide reports and estimates for human rights conditions
in different countries. While one singular supplier might perform better (or worse), we assume that these estimates provide an
indication how likely it is that human rights were violated during the production of materials in this country.
<!--
    We distinguish between X different impact categories and collect data for each. The impact categories are described in more detail here.
-->

<h2 id="calculation">Calculation of Activity Values, Risks and Hotspots</h2>

<h3>Activity Value</h3>

Based on our basic concepts and assumptions explained above, we calculate an activity value (share of total product weight) that can be 
associated to each component, material and country. In particular, this means that
<ul>
<li>
An activity value of 10% for a component means that this component contributes
10% to the total weight of a product.
</li>
<li>
An activity value of 10% for a material means that this material contributes 10% to the total product weight
(it may be part of various different components in the product).
</li>
<li>
An activity value of 10% for a country means, that the raw materials
produced in this country contribute 10% to the total product weight (different raw materials may be produced in this country).
</li>
</ul>

<h3>Risk Value</h3>

<!-- TODO: add examples-->
For each social indicator, the values are sorted and the highest 25% of values are interpreted as "High Risk", the lowest 25% of values as "Low Risk",
and everything in between as "Medium Risk" (depending on the indicator interpretation, this may also be the other way round, with lowest values as "High Risk",
and highest values as "Low Risk"). Initially these risk qualifications are only connected to countries, but via our assumed distribution
of material production and component composition, they can be associated to materials and components as well. So, a component that is partly
made of a material that is to a certain percentage produced in a "High Risk"-country for a specific indicator, is seen as "High Risk"-component.

<h3>Hotspots</h3>

Hotspots are those countries, components and materials that show the highest activity and highest risks. For each component, we highlight
the two components that show high risk and have the highest activity as hotspots. If no components show high risk, we highlight the two medium risk
components with highest activity etc. The same procedure applies for material and country hotspots.

<h2 id="impact-categories">Impact Categories</h2>

We follow the Social Life Cycle Assessment (S-LCA) <a href="https://www.lifecycleinitiative.org/wp-content/uploads/2012/12/2009%20-%20Guidelines%20for%20sLCA%20-%20EN.pdf">guidelines</a> and 
<a href="https://www.lifecycleinitiative.org/wp-content/uploads/2013/11/S-LCA_methodological_sheets_11.11.13.pdf">methodological sheets</a> for identifying potential social impact categories. Currently we
focus on workers as stakeholder group. In particular, we collect data for the impact categories <b>Freedom of Association and Collective Bargaining</b>,
<b>Child Labor</b>, <b>Fair Salary</b>, <b>Hours of Work</b>, <b>Forced Labor</b>, <b>Equal Opportunities / Discrimination</b>,
<b>Health and Safety</b>, <b>Social Benefit / Social Security</b>. 
Initially, we have defined and collected data for one indicator per impact category.
In the following, we cite the definition for each impact category from the S-LCA methodological sheets, as these 
form the foundation for our understanding and data collection of "Fairness". For more details and explanations,
please refer to the methodological sheets.

<h3>Freedom of Association and Collective Bargaining</h3>
<p>
All workers and employers have the right to establish and to join organizations of their choice, without prior authorization, to promote and defend their respective interests, and to negotiate collectively with other parties. They should be able to do this freely, without interference by other parties or the state, and should not be discriminated as a result of union membership.
</p>

<p>
<b>Indicator:</b>
The indicator is "Share of employees covered by one or more collective agreement (in percent), latest year)" as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/collective-bargaining/ (accessed 10 2019).
The individual values per country stem from different years, the oldest from 2008.
We used the list as is, only removing unneeded columns "MEASURE" and "TIME_PERIOD".
</p>

<h3>Child Labor</h3>
<p>
The term “child labour” is often defined as work that deprives children of their childhood, their potential and their dignity, and that is harmful to physical and mental development. It refers to work that is:
<ol>
<li>Mentally, physically, socially or morally dangerous and harmful to children;</li>
<li>Depriving them of the opportunity to attend school;</li>
<li>Obliging them to leave school prematurely; or</li>
<li>Requiring them to attempt to combine school attendance with excessively long and heavy work.</li>
</ol>
In its most extreme forms, child labour involves children being enslaved, separated from their families, exposed to serious hazards and illnesses and/or left to fend for themselves on the streets of large cities –   often at a very early age.
</p>

<p>
<b>Indicator:</b>
The indicator is "Share of children engaged in economic activity and household chores (in percent), latest year" as provided by ILOSTAT in the infographic in https://ilostat.ilo.org/topics/child-labour/ (accessed 10 2019).
The individual values per country stem from different years, the oldest from 2010. Some values are given for ages 5 to 17, others for ages 5 to 14.
The provided list distinguishes between values for female, male and total. We only used the values for total.
</p>
<h3>Fair Salary</h3>
<p>
Fair wage means a wage fairly and reasonably commensurate with the value of a particular service or class of service rendered, and, in establishing a minimum fair wage for such service or class of service. 
</p>

<p>
<b>Indicator:</b>
The indicator is "Share of employment by economic class (in percent), ILO modelled estimates for 2018, with lowest economic class based on the World Bank's international poverty line of $1.90 a day (using 2011 PPPs)", as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/working-poor/ (accessed 10 2019).
We only used the values for "Extremely poor".
</p>
<h3>Hours of Work</h3>
<p>
The hours of work comply with applicable laws and industry standards. Workers are not on a regular basis required to work in excess of 48 hours per week and have at least one day off for every 7-day period. Overtime is voluntary, does not exceed 12 hours per week, is not demanded on a regular basis and is compensated at a premium rate. The needs and expectations of the workers are taken into account in the organisation of working hours. There are also higher restrictions if the hours of work are made during the night. 
</p>

<p>
<b>Indicator:</b>
The indicator is "Share of employed persons working 40 or more hours per week, latest year", as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/working-time/ (accessed 10 2019).
The individual values per country stem from different years, the oldest from 2015.
We only used the values for "49 or more hours per week" and dropped "40 to 48 hours per week".
</p>
<h3>Forced Labor</h3>
<p>
Forced or compulsory labour is any work or service that is exacted from any person under the menace of any penalty, and for which that person has not offered himself or herself voluntarily. Providing wages or other compensation to a worker does not necessarily indicate that the labour is not forced or compulsory. By right, labour should be offered voluntary and workers should be free to leave the employment at any time in accordance with established rules.
</p>

<p>
<b>Indicator:</b>
The indicator is "Estimated prevalence of modern slavery by country (victims per 1,000 population)" as provided by the Global Slavery Index 2018 Dataset, Walk Free Foundation, available from https://www.globalslaveryindex.org. 
The individual values per country are based on surveys carried out in 48 countries of the 167 countries provided. The outcomes were extrapolated by the Walk Free Foundation “to countries with an equivalent risk profile” for the remaining. Some values are missing.
</p>
<h3>Equal Opportunities / Discrimination</h3>
<p>
Everybody deserves a "fair chance". It doesn't matter what sex, race or age you are, if you have a disability, your marital status, whether you are pregnant, your family status or your family responsibilities, the religious or political beliefs you might hold and your sexual orientation. Everybody has the right to be treated fairly and access to equal opportunities.Equal opportunity or the principle of non-discrimination emphasizes that opportunities in education, employment, advancement, benefits and resource distribution, and other areas should be freely available to all citizens irrespective of their age, race, sex, religion, political association, ethnic origin, or any other individual or group characteristic unrelated to ability, performance, and qualification.
</p>

<p>
<b>Indicator:</b>
The indicator is "Proportion of women in managerial positions (%)" as provided by ILOSTAT under https://ilostat.ilo.org/data/ (5.5.2 Women in management, Last update 30Sep19).
The Excel file provides multiple entries for many countries with varying source type and time. We only used the latest available value per country for "Total management" (dropping "senior and middle management column).
</p>
<h3>Health and Safety</h3>
<p>
Occupational health should aim at: the promotion and maintenance of the highest degree of physical, mental and social well-being of workers in all occupations; the prevention amongst workers  of  departures  from  health  caused  by  their  working  conditions;  the  protection  of  workers  in  their  employment  from  risks  resulting  from  factors  adverse  to  health;  the  placing  and maintenance of the worker in an occupational environment adapted to his physiological and psychological capabilities; and, to summarize, the adaptation of work to man and of each man to his job.
</p>

<p>
<b>Indicator:</b>
The indicator is "Fatal occupational injuries per 100'000 workers" as provided by ILOSTAT under https://ilostat.ilo.org/data/ (accessed 10 2019).
The Excel file provides multiple entries for Source and  Time per country. We selected the latest available value. We only used the values for "Total" and "Fatal" (dropping the distinction between male/female and non-fatal injuries).
</p>
<h3>Social Benefit / Social Security</h3>
<p>
Social benefits refer to non-monetary employment compensation. Four basic categories of Social Security benefits are often included and are paid based upon the record of worker’s earnings: Retirement, disability, dependents, and survivors benefits.
</p>

<p>
<b>Indicator:</b>
The indicator is "Share of population covered by at least one social protection benefit (in percent), latest year" as provided by ILOSTAT in the infographic under https://ilostat.ilo.org/topics/social-protection/ (accessed 10 2019).
The individual values per country stem from different years, the oldest from 2016.
</p>

<h2 id="component-materials">Data for Component Materials</h2>

We retrieved data on the material composition of the components from full material declarations by manufacturers, published of their websites.
<table>
    <tr>
        <th>Component</th>
        <th>Source</th>
        <th>Date</th>
    </tr>
    <tr><td> Aluminium Electrolytic Capacitor SMD 6.3x5.5 </td><td> FMD for <a href="https://www.niccomp.com/resource/files/ipc/NACExxx6.3x5.5.pdf">NACE 6.3x5.5 Series</a> by NIC Components </td><td> 2004-10-01 </td></tr>
    <tr><td> Aluminium Electrolytic Capacitor SMD 10x8 </td><td> FMD for <a href="https://www.niccomp.com/resource/files/ipc/NACExxx10x8.pdf">NACE 10x8 Series</a> by NIC Components </td><td> 2004-10-01 </td></tr>
    <tr><td> Multilayer Ceramic Capacitor SMD 0603 Class I BME </td><td> FMD for <a href="https://www.niccomp.com/resource/files/ipc/NMC0603NPO%20Class%206.pdf">NMC NP0 0603 Series</a> by NIC Components </td><td> 2004-10-01 </td></tr>
    <tr><td> Multilayer Ceramic Capacitor SMD 0603 Class II BME </td><td> FMD for <a href="https://www.niccomp.com/resource/files/ipc/NMC0603X5R%20Class%206.pdf">NMC X5R 0603 Series</a> by NIC Components </td><td> 2004-10-01 </td></tr>
    <tr><td> Zener Diode SOD-123 </td><td> FMD for <a href="https://www.onsemi.com/PowerSolutions/MaterialComposition.do?export=pdf&opnId=MMSZ10T3G">MMSZ10: 500 mW 10 V ±5% Zener Diode Voltage Regulator</a> by ON Semiconductors </td><td> 2020-01-30 </td></tr>
    <tr><td> Zener Diode SOD-523 </td><td> FMD for <a href="https://www.onsemi.com/PowerSolutions/MaterialComposition.do?export=pdf&opnId=SZMM5Z9V1T1G">MM5Z2V4: 500 mW Standard Tolerance Zener Diode Voltage Regulator</a> by ON Semiconductors </td><td> 2020-01-30 </td></tr>
    <tr><td> Small Signal Schottky Diode SOD-323 </td><td> FMD for <a href="https://www.nexperia.com/products/diodes/automotive-diodes/automotive-schottky-diodes-and-rectifiers/automotive-general-purpose-schottky-diodes-250-ma/BAT46WJ.html">BAT46WJ Schottky barrier diode</a> by nexperia (Ex-NXP, Ex-Philips) </td><td> 2018-04-25 </td></tr>
    <tr><td> Schottky Barrier Rectifier Diode DO-214AC </td><td> FMD for <a href="https://www.mccsemi.com/ProductCategories/ProductDetails?productName=SK54A-L&productType=SchottkyBarrierRectifier">SK54A-L Schottky Barrier Rectifier</a> by MCC </td><td> 2016-12-20 </td></tr>
    <tr><td> PTC Resettable Fuse SMD 2920 </td><td> FMD for <a href="https://www.bourns.com/products/circuit-protection/resettable-fuses-multifuse-pptc/product/MF-LSMF">MF-LSMF185/33X</a> by Bourns </td><td> 2010-12-15 </td></tr>
    <tr><td> Ferrite Chip Bead SMD 0603 </td><td> FMD for <a href="https://www.bourns.com/products/magnetic-products/chip-beads/product/MH">MH1608</a> by Bourns </td><td> 2018-04-17 </td></tr>
    <tr><td> Standard Rectifier Diode DO-41 </td><td> FMD for <a href="https://www.onsemi.com/products/discretes-drivers/diodes-rectifiers/rectifiers/1n4001">1N4001G</a> by ON Semiconductor </td><td> 2020-02-07 </td></tr>
    <tr><td> Standard Rectifier Diode DO-214AC </td><td> FMD for <a href="https://www.niccomp.com/products/pSeries.php?pSeries=NRD">NRD Series</a> by NIC Components </td><td> 2008-08-15 </td></tr>
    <tr><td> Metallized polyester (PET/MKT) film capacitor, 5mm lead spacing, radial boxed, 2.5×6.5×7.3 </td><td> FMD for <a href="https://www.tdk-electronics.tdk.com/inf/20/20/db/fc_2009/B32520_529.pdf">MKT FILM (Boxed) B32529C1104Mxxx</a> </td><td> 2019-11-18 </td></tr>
    <tr><td> Bipolar Electrolytic Capacitor Radial 5,5 x 12,0 mm </td><td> Umbrella Spec for <a href="https://www.frolyt.de/wp-content/uploads/EKSU-deutsch-07-2019.pdf">EKSU</a> from Frolyt </td><td> 2019-08-12 </td></tr>
    <tr><td> Thick Film Chip Resistor 0603 </td><td> FMD for <a href="https://www.bourns.com/products/fixed-resistors/thick-film-chip-resistors/product/CR0603">CR0603 Series</a> by Bourns </td><td> 2003-01-04 </td></tr>
    <tr><td> Single-ended USB-A cable AWG24 2.0m </td><td> FMD for <a href="https://www.te.com/usa-en/product-1487593-1.html">USB Cacle Assembly</a> by TE Connectivity </td><td> 2020-01-24 </td></tr>
    <tr><td> Single-ended USB-A cable AWG24 1.3m </td><td> calculated from 2m variant </td><td> 2020-01-24 </td></tr>
    <tr><td> Mechanical Incremental Rotary Encoder 12mm Shaftless </td><td> FMD for <a href="https://www.bourns.com/products/encoders/contacting-encoders?product=PES12">PES12 Encoder</a> by Bourns<td> 2008-06-23 </td></tr>
    <tr><td> Ultra Miniature Micro Switch with Lever 13x7x4mm Through Hole </td><td> FMD for <a href="https://www.te.com/usa-en/product-1825043-3.html">UP01DTANLA04 micro switch</a> by TE Connectivity </td><td> 2017-10-11 </td></tr>
    <tr><td> Ultra Miniature Micro Switch without Lever 13x7x4mm Through Hole </td><td> calculated from variant with lever </td><td> 2017-10-11 </td></tr>
    <tr><td> Solder Wire HS10 Fair </td><td> Supply chain for <a href="https://fairloetet.de/wp-content/uploads/2018/10/lieferkette_fairloetet_1115.pdf">HS10 Fair</a> by Stannol / FairLötet e.V. </td><td> 2018-10-01 </td></tr>
    <tr><td> Solder Paste Sn96.5Ag3Cu0.5 (SAC) </td><td> Safety Data Sheet for <a href="https://aimsolder.com/sites/default/files/rel22_m8-ghs_america-english.pdf">M8 REL22</a> by AIM Solder </td><td> 2017-11-16 </td></tr>
    <tr><td> General Simple Logic IC in P-DIP with 8 pins </td><td> FMD for <a href="http://www.ti.com/materialcontent/en/report?pcid=10896&opn=CD40107BEE4">CD40107BEE4</a> by Texas Instruments </td><td> 2020-02-20 </td></tr>
    <tr><td> 1-layer PCB with FR-4 laminat and NiAu (chem. gold) finish </td><td> <a href="https://gitlab.com/fairloetet/pse/-/blob/develop/data/input/components/README.md">Fairtronics' calculation</a> </td><td> 2020-02-20 </td></tr>
    <tr><td> IC BGA 256 17x17x1.41mm </td><td> FMD for <a href="http://www.ti.com/materialcontent/en/report?pcid=224722&opn=TMS320C28346ZFETR">TMS320C28346ZFETR</a> by Texas instruments </td><td> 2020-02-26 </td></tr>
    <tr><td> IC PLCC 44 16.6x16.6x4.57mm </td><td> FMD for <a href="http://www.ti.com/materialcontent/en/report?pcid=43776&opn=SN74ACT8990FN">SN74ACT8990FN</a> by Texas Instruments </td><td> 2020-02-26 </td></tr>
    <tr><td> IC SO 20 12.8x7.5x2.65mm </td><td> FMD for <a href="http://www.ti.com/materialcontent/en/report?pcid=17372&opn=CDC208NSR">CDC208NSR</a> by Texas Instruments </td><td> 2020-02-26 </td></tr>
    <tr><td> IC TQFP 32 5x5x1.0mm </td><td> FMD for <a href="http://www.ti.com/materialcontent/en/report?pcid=81864&opn=TLV320AIC1103PBSR">TLV320AIC1103</a> by Texas Instruments </td><td> 2020-02-26 </td></tr>
    <tr><td> IC TSSOP 48 6.1x12.5x1.2mm </td><td> FMD for <a href="http://www.ti.com/materialcontent/en/report?pcid=226084&opn=MSP430FR4133IG48R">MSP430FR4133IG48R</a> by Texas Instruments </td><td> 2020-02-26 </td></tr>
    <tr><td> Resistor THT Metal film 4.5x11mm + 28mm axial </td><td> FMD for <a href="https://www.niccomp.com/resource/files/ipc/NMO100xxxx.pdf">NMO100 Series</a> by NIC Components </td><td> 2008-08-13 </td></tr>
    <tr><td> Transistor 15V NPN SOT-23 with 3 leads </td><td> FMD for <a href="https://www.diodes.com/assets/Part-Support-Files/FMMT617/MDSSOT23eu.pdf">FMMT617</a> by Diodes Inc. </td><td> 2007-08-01 </td></tr>
    <tr><td> Lead double-ended pre-crimped with tin-plated nano-fit female connectors 20AWG 150mm </td><td> FMD for <a href="https://www.molex.com/molex/products/part-detail/cable_assemblies/0797582139">797582139</a> by Molex </td><td> 2020-04-17 </td></tr>
    <tr><td> Power Connector dual row male with 20 2.5mm circuits </td><td> FMD for <a href="https://www.molex.com/molex/products/part-detail/crimp_housings/1053081210">105308-1210</a> by Molex </td><td> 2020-04-24 </td></tr>
    <tr><td> Flexible flat jumper cable (FFC) with 33 0.5mm circuits opposite side and tin plating 5.1cm </td><td> FMD for <a href="https://www.molex.com/molex/products/part-detail/cable/0982661032">98266-1032</a> by Molex </td><td> 2017-05-03 </td></tr>
    <tr><td> IC SOT-23 with 5 pins and tin finish 2.9x1.6x1.45 </td><td> FMD for <a href="https://www.ti.com/store/ti/en/p/product/?p=TLV75533PDBVR">TLV75533PDBVR</a> by Texas Instruments </td><td> 2020-04-26 </td></tr>
    <tr><td> IC (MCU) TQFP 32 7x7x1.0mm with tin finish </td><td> FMD for <a href="https://www.microchip.com/wwwproducts/en/ATmega32U4">ATmega32U2-AU</a> by Microchip </td><td> 2015-08-17 </td><</tr>
    <tr><td> IC (MCU) TQFP 44 10x10x1.0mm with tin finish </td><td> FMD for <a href="https://www.microchip.com/wwwproducts/en/ATmega32U4">ATmega32U2-AU</a> by Microchip </td><td> 2015-08-17 </td></tr>
    <tr><td> Standard Diode 100V SOD-323 </td><td> FMD for <a href="https://www.onsemi.com/products/discretes-drivers/diodes-rectifiers/small-signal-switching-diodes/smmdl914t1g">MMDL914T1G</a> by ON Semiconductor </td><td> 2020-05-05 </td></tr>
    <tr><td> SMD Crystal with 3.2x 2.5x 0.8mm ceramic package </td><td> FMD for <a href="https://ecsxtal.com/ecx-32">ECX-32</a> by ECS Inc. </td><td> ? </td></tr>
    <tr><td> IC (Logic) thermally enhanced QFP 64 10x10x1.2mm </td><td> FMD for <a href="https://www.ti.com/product/TUSB8041-Q1">TUSB8041IPAPR-Q1</a> by Texas Instruments </td><td> 2020-05-15 </td></tr>
    <tr><td> IC (Logic) TOSOP 20 4.4x6.5x1.15mm </td><td> FMD for <a href="https://www.ti.com/product/TXS0108E">TXS0108EPWR</a> by Texas instruments </td><td> 2020-05-27 </td></tr>
    <tr><td> IC (Power Management) TO-236 (D2PAK) 7 pin 10.1x8.9x4.5mm </td><td> FMD for <a href="https://www.ti.com/product/LM2677">LM2677</a> by Texas Instruments </td><td> 2020-06-05 </td></tr>
    <tr><td> IC (Amplifier) TSSOP 16 5x4.4x1mm </td><td> FMD for <a href="https://www.ti.com/store/ti/en/p/product/?p=INA260AIPWR">INA260AIPWR</a> by Texas Instruments </td><td> 2020-06-19 </td></tr>
    <tr><td> IC (Power Management) MSOP 8 3x3x0.8mm </td><td> FMD for <a href="https://www.analog.com/en/products/ltc4359.html">LTC4359IMS8#PBF</a> by Analog Devices </td><td> 2020-07-11 </td></tr>
    <tr><td> IC (Power Management) SSOP 44 13x5.3x1.7mm </td><td> FMD for <a href="https://www.analog.com/en/products/ltc6803-4.html">LTC6803IG-4#PBF</a> by Analog Devices </td><td> 2020-07-11 </td></tr>
    <tr><td> IC (Clock and Timing) TSSOP 20 4.4x6.5x1mm </td><td> FMD for <a href="https://www.idt.com/eu/en/products/clocks-timing/application-specific-clocks/pci-express-clocks/pci-express-clock-generators/5v41066-4-output-pcie-gen12-synthesizer">5V41066</a> by Renesas/IDT </td><td> 2008-09-23 or 2011-03-15 </td></tr>
    <tr><td> IC (Curcuit Protection) TSSOP 38 9.7x4.4x0.9mm </td><td> FMD for <a href="https://www.onsemi.com/products/isolation-protection-devices/esd-protection-diodes/cm2020-00tr">CM2020-00TR</a> by ON Semiconductor </td><td> 2020-08-14 </td></tr>
    <tr><td> IC (Linear) TQFN 16 5.10mm x 5.10mm x 0.80mm </td><td> FMD for <a href="https://www.maximintegrated.com/en/products/analog/audio/MAX9715.html">MAX9715ETE+</a> by Maxim Integrated </td><td> 2020-08-20 </td></tr>
    <tr><td> IC (Interface) SSOP 28 10.5mm x 5.6mm x 1.85mm </td><td> FMD for <a href="https://www.microchip.com/wwwproducts/en/MCP3919">MCP3919A1-E/SS</a> by Microchip </td><td> ? </td></tr>
    <tr><td> IC (Power Management) QFN 28 4.0x4.0x0.75mm </td><td> FMD for <a href="https://www.analog.com/en/products/ltc4162-f.html">LTC4162-F</a> by Analog Devices </td><td> 2020-07-11 </td></tr>
    <tr><td> IC (Power Management) SOT-223 6.5x3.5x1.6mm </td><td> FMD for <a href="https://www.ti.com/store/ti/en/p/product/?p=TLV1117-15CDCYR">TLV1117-15CDCYR</a> by Texas Instruments </td><td> 2020-08-28 </td></tr>
    <tr><td> IC (MCU) LQFP 48 7.0x7.0x1.4mm </td><td> FMD for <a href="https://www.nxp.com/products/processors-and-microcontrollers/arm-microcontrollers/general-purpose-mcus/lpc1100-cortex-m0-plus-m0/scalable-entry-level-32-bit-microcontroller-mcu-based-on-arm-cortex-m0-plus-and-cortex-m0-cores:LPC11U00">LPC11U24FBD48</a> by NXP </td><td> 2020-04-11 </td></tr>
    <tr><td> IC (Power Management) DFN 10 3.0x3.0x0.9mm </td><td> FMD for <a href="https://www.ti.com/product/TPS2561">TPS2561DRCR</a> by Texas Instruments </td><td> 2020-09-18 </td></tr>
    <tr><td> Power Connector DC Power Jack 2.5mm 13.3x13.3x9.0mm </td><td> FMD for <a href="https://www.te.com/usa-en/product-6643220-1.html">6643220-1</a> by TE Connectivity </td><td> 2016-07-02 </td></tr>
    <tr><td> Card Connector M.2 M-key 67 pins 4.2x21.9x8.7mm </td><td> FMD for <a href="https://www.te.com/usa-en/product-1-2199230-6.html">1-2199230-6</a> by TE Connectivity </td><td> 2019-11-18 </td></tr>
</table>

<h2 id="material-countries">Data for Materials from Countries</h2>

The material's world production share of different countries has been obtained from https://www.usgs.gov/centers/nmic/commodity-statistics-and-information by the U.S. Geological Survey.
For quite a few materials, it provides "Mineral Commodity Summaries" annually, which contain data on "World Mine Production". 
For aluminium we took bauxite, and for iron we took iron ore as a proxy.

<h2 id="future-developments">Future Developments</h2>

We consider the current state of Fairtronics to be in Prototype status. Possible future steps and extensions are:

<ul>
<li>Evaluation of current calculation according to S-LCA guidelines</li>
<li>Extension of data base to cover more components and materials</li>
<li>Extension of data base to cover further stakeholder groups (local community, consumer, ...) </li>
<li>Extension of data base and calculation to further life cycle stages (assembly of components, assembly of final product, disposal, ...)</li>
</ul>
