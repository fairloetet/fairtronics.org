---
layout: page-with-outline.liquid
pageTitle: How it works
pageSubtext: Explanation of our concepts and methods
outline: 
    - {url: '#introduction', title: 'Introduction'}
    - {url: '#basic-concepts', title: 'Basic Concepts'}
    - {url: '#calculation', title: 'Calculation'}
---
<h2 id="introduction">Introduction</h2>
<p>
By analyzing the raw materials contained within a device, we pinpoint the global hotspots where human rights violations are most likely to occur.
Our goal is to provide a risk analysis that highlights the areas that should be focused for further activities, like further data collection, audits of suppliers, 
or exploration of design alternatives. 
In the following, we describe the basic concepts, assumptions, calculations and limitations behind Fairtronics.
</p>

<h2 id="basic-concepts">Basic Concepts</h2>

<h3>Electronics Devices consist of Parts</h3>

Within Fairtronics, <b>devices</b> contain a number of <b>parts</b> from our database, where we collect data about electronics components (like resistors, circuit boards, cables, ...). A part may be included more than once in a device, e.g. typically quite a few ceramic capacitors of SMD type can be found.

<h3>Parts are made from Materials</h3>
<p>
For each <b>part</b> in our database, we collect data about its total weight and the share in weight of the <b>materials</b> it is made of.
For example, a cable within a device can be made of copper conductors and an isolation or jacket of PVC plastic. 
</p><p>
We usually collect this data based on full material declarations provided by manufacturers, but often we need to fall back on data of more or less similar parts.
In any way, for all parts in our database, the material composition is known.
</p>

<h3>Materials are produced from Resources</h3>
<p>
Since we are interested in raw material extraction, our database knows on how to process <b>materials</b> from natural <b>resources</b>.
While copper is smeltered and refined from copper ore extracted in mines, PVC is made of the resources crude oil, natural gas, and salt.
</p><p>
Information of processing resources to materials are taken from the <a href="https://ecoinvent.org/">ecoinvent</a> life-cycle assessment database.
</p><p>
In most cases, much more resources are required in weight than material outcome, therefore a cable of only a few grams might be produced of nearly a kilogram of resources.
You have to bear in mind, that when we are taking of the weight of a part, a material weight as well a resource weight needs to be taken into account.
</p>

<h3>Resources are extracted in different Countries</h3>
<p>
For nearly each <b>resource</b>, we collect data about world production share of different <b>countries</b>.
So, in effect, we assume that for example the crude oil for any part's material of a device is a mix of all crude oil sources in the world. 
Our data is based on country's raw materials agencies, collected in the figures of the <a href="https://www.world-mining-data.info/">World Mining Data</a>
</p>

<h3>Countries show different risks concerning Social Impacts</h3>
<p>
Global institutions like the International Labor Organization (ILO) or the Unicef provide reports and estimates for human rights conditions
in different countries. While one singular supplier might perform better (or worse), we assume that these estimates provide an
indication how likely it is that human rights were violated during the production of resources in this country.
</p><p>
We still measure the amount of social risk on weight, i.e. with a cable made from some hundred grams of resources, only few might be based of forced labour.
You should be aware that weight is only a proxy for what we are actually interested in: working hours or potential for human harm. As long as we are missing better data sources, we assume that  material weight corresponds to the human effort and therefore potential of harm that can be associated with this material.
</p><p>
We currently support three impact categories:
<ul>
<li>Child Labour, with data provided by the World Bank in its <a href="https://databank.worldbank.org/source/world-development-indicators">World Development Indicators</a>, that is:
    <ul>
        <li>Children in employment, total (% of children ages 7-14)</li>
        <li>Average working hours of children, working only, ages 7-14 (hours per week)</li>
        <li>Labor force participation rate, total (% of total population ages 15-64) (modeled <a href="https://ilostat.ilo.org/">ILO</a> estimate)</li>
        <li>Population ages 0-14, total</li>
        <li>Population ages 15-64, total</li>
    </ul>
</li>
<li>Forced Labour, with data provided by the Walk Free Foundation's <a href="http://www.globalslaveryindex.org">Global Slavery Index</a> in its <i>Estimated prevalence of modern slavery by country</i></li>
<li>Wage risk, expressing the difference of a country's wage in the extration business (data provided by ILO) from a decent living wage (data provided by <a href="https://www.globallivingwage.org/">Global Living Wage Coalition</a> and <a href="https://www.valuingnature.ch/">Valuing Nature</a>), combined with World Bank's data on countries' inflation rates to convert between non-aligning dates.</li>
</ul>
</p>

<h2 id="calculation">Calculation of Activity Values and Risks</h2>
<p>
Based on our basic concepts and assumptions explained above, we calculate an activity value (share of total product weight) that can be 
associated to each part, material, resource and country. In particular, this means that
<ul>
<li>
An activity value of 10% for a part means that this part contributes
10% to the total weight of a product.
</li>
<li>
An activity value of 10% for a material means that this material contributes 10% to the total product weight
(it may be part of various different parts in the device).
</li>
<li>
An activity value of 10% for a resource means that this resource contributes 10% to the total product's resource weight
(remember that the weight is now measured in resources, not materials anymore).
</li>
<li>
An activity value of 10% for a country means, that the resources
produced in this country contribute 10% to the total product's resource weight (different resources may be produced in this country).
</li>
</ul>
</p>

